export const SUBSCRIPTION_STATUS_ACTIVE = 'active';
export const SUBSCRIPTION_STATUS_PAUSED = 'paused';
export const SUBSCRIPTION_STATUS_CANCELLED = 'cancelled';
export const SUBSCRIPTION_STATUS_SUSPENDED = 'suspended';
