export const MIN_ORDER_VALUE = 39;
export const FREE_SHIPPING = 59;
export const DISCOUNT_5 = 79;
export const DISCOUNT_10 = 99;
export const REFER_FRIEND_SENDER_BONUS = 20;
export const REFER_FRIEND_RECEIVER_BONUS = 20;
